#!flask/bin/python
from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/")
def index():
	return "Hello, World!"

@app.route("/helloworld/api/v1.0/helloworld", methods=['GET'])
def get_hello_word():
	return jsonify({''})

if __name__ == '__main__':
	app.run(debug=True)


messages = [
    {
        'id': 1,
        'greeting_message': u'Hello World',
    },
]

@app.route("/helloworld/api/v1.0/messages", methods=['GET'])
def get_messages():
    return jsonify({'tasks': messages})

if __name__ == '__main__':
    app.run(debug=True)