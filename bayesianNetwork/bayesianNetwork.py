import pandas as pd
from pathlib import Path
from pomegranate import *


class Directory:
    def __init__(self, source_folder, target_folder, input_file):
        self.source_folder = source_folder
        self.target_folder = target_folder
        self.input_file = input_file
        self.output_file = None

    def read_file(self):
        df = pd.read_csv(self.source_folder / self.input_file, sep=',')
        return df

    def write_file(self, output_text, output_file):
        self.output_file = output_file
        target = Path(self.target_folder / self.output_file)
        target.write_text(output_text)
        print("Create output file:" + self.output_file)


class Data:
    def __init__(self, df):
        self.df = df

    def get_data(self, columns):
        df = self.df[columns]
        data_set = df
        return data_set


class BayesianNetworkModel():
    def __init__(self, data_set):
        self.data_set = data_set
        self.model = None

    def train_model(self):
        self.model = BayesianNetwork.from_samples(self.data_set,
                                                  state_names=['category',
                                                               'type',
                                                               'subtype'],
                                                  root=0,
                                                  reduce_dataset=True,
                                                  algorithm='chow-liu')
        return self.model

    def plot_model(self):
        self.model.plot()

    def predict_proba_model(self, X):
        beliefs = self.model.predict_proba(X)
        print ("\n".join( "{}\t{}".format( state.name, str(belief) )
                for state, belief in zip( self.model.states, beliefs)))

    def predict_model(self, X):
        return self.model.predict([X])



def predict_network(data_set, test_case):
    model = BayesianNetworkModel(data_set)
    model.train_model()
    #model.predict_proba_model(test_case)
    prediction = model.predict_model(test_case)
    return prediction



def main():
    source_folder = Path("./input")
    target_folder = Path("./output")
    input_file = 'NetworkTestCases.csv'
    columns = ['category', 'type', 'subtype']
    testCase1 = {'category': 'category01',
                 'type': 'type01'}
    testCase2 = {'subtype': 'subtype10'}
    testCase3 = {'subtype': 'subtype99'}

    d = Directory(source_folder, target_folder, input_file)
    df = d.read_file()

    data = Data(df)
    data_set = data.get_data(columns)
    prediction = predict_network(data_set, testCase1)

    print(data_set)
    print(test_case)


    text = "Input:" + " " + str(testCase1) + "  Output: " + " " + str(prediction)
    d.write_file(text, "testCase1.txt")

    prediction = predict_network(data_set, testCase2)
    text = "Input:" + " " + str(testCase2) + "  Output: " + " " + str(prediction)
    d.write_file(text, "testCase2.txt")

    prediction = predict_network(data_set, testCase3)
    text = "Input:" + " " + str(testCase3) + "  Output: " + " " + str(prediction)
    d.write_file(text, "testCase3.txt")



if __name__ == '__main__':
    main()
